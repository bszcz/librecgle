#!/bin/sh

# Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
# This program is free software under the MIT license.

tools="clang gcc git make"
for tool in $tools; do
	echo -n "checking for '$tool': "
	$tool --version | head -n1
done

echo

libs="libfftw3.so libfftw3_threads.so libgomp.so libm.so libpng[0-9]*.so libpthread.so libz.so"
for lib in $libs; do
	echo -n "checking for '$lib': "
	ldconfig -p | grep -E "$lib" > /dev/null
	if [ $? -eq 0 ]; then
		echo "found"
	else
		echo "missing"
	fi
done
