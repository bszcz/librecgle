LibreCGLE [![Build Status](https://travis-ci.org/bszcz/librecgle.svg?branch=master)](https://travis-ci.org/bszcz/librecgle)
=========

Pseudo-spectral solver for the Complex Ginzburg-Landau Equation (CGLE).

Quick Start
-----------

### Download
Download LibreCGLE using `git` with

    $ git clone https://gitlab.com/bszcz/librecgle.git --recursive

this creates `librecgle` directory and downloads `datum` and `pixmap` submodules (via the `--recursive` flag) needed by LibreCGLE.

### Dependecies
Running the `linux_check.sh` script can be useful at identifying any missing dependencies. Most libraries (`libz`, `libpng`) and tools (`gcc`, `make`) should be present in Linux, however, the FFTW3 library will most likely need installation. To install it on Debian/Mint/Ubuntu issue

    $ sudo apt-get install libfftw3*

and similarly on CentOS/Fedora/RedHat

    $ sudo yum install fftw fftw-devel

### Building
Compilation can be done by

    $ make

which builds five programs: `cgl_init`, `cgl_step`, `cgl_stat`, `cgl_image` and `cgl_test` in the `bin` directory. Each tool except `cgl_stat` accepts the `--config` flag (or `-c` for short) followed by a path to the configuration file. In addition, `cgl_stat` and `cgl_image` expect the `--files` flag (or its short version `-f`) followed by the data files to analyse or visualise respectively. This compilation should be followed by running

    $ ./bin/cgl_test --config src/cgl_test.conf

to see if all the unit tests are passed. (the line with `cgl_test_dealias:` should be followed by a rectangle filled with 0s, with four rectangles filled with 1s in each corner and 2s as their border)

### Solving the CGLE

To initialise the domain, use the `cgl_init` program

    $ ./bin/cgl_init --config src/cgl.conf

which should create a file called `cgl__L512x256_G512x256__D1.00_b0.00_c1.00__t000000_.datum.gz` in the `data` directory. To learn about the configuration, see `src/cgl.conf` which includes comments about the available options. In addition, the options can be specified as command line arguments in `option=value` format (no leading `-`'s are required). To integrate these initial conditions, use the `cgl_step` binary

    $ ./bin/cgl_step --config src/cgl.conf

which integrates the domain up to t = 100 in steps of dt = 10, as specified by the `src/cgl.conf` configuration file. Finally, analysing and visualising the data can be done with the `cgl_stat` and `cgl_image` tools

    $ ./bin/cgl_image --config src/cgl.conf --files data/*.datum.gz
    $ ./bin/cgl_stat --files data/*.datum.gz

which create three histogram files and five images for each `.datum.gz` file, writing them to the `data` directory. In addition, the output of the `cgl_stat` tool lists average modulus squared, most common modulus squared from the histograms and core areas for different cutoffs as specified by the header of the output. Setting one of the grid sizes to 1 creates one dimensional images which can be montaged to show time evolution. For example, for gridy = 1, the command would be

    $ montage -tile 1 -geometry 512x1+0+0 data/*G512x1*arg_hue*png data/cgl_1D_montage.png

COPYRIGHT / LICENSE
-------------------

    Pseudo-spectral solver for the Complex Ginzburg-Landau Equation (CGLE).
    Copyright (c) 2013-2015 Bartosz Szczesny

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
