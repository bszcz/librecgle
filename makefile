FLAGS=-g -std=c99 -Wall -Wextra -O3 -fopenmp -I/usr/lib/gcc/x86_64-linux-gnu/4.4/include -I/usr/lib/gcc/x86_64-linux-gnu/4.6/include -I/usr/include
VPATH=src:src/datum:src/pixmap

all: cgl_init cgl_step cgl_image cgl_stat cgl_test

datum_deps=obj/datum.o obj/datum_gz.o -lz
etd_deps=obj/etd_create.o obj/etd_step.o -lfftw3_threads -lfftw3 -lm
pixmap_deps=obj/pixmap.o obj/pixmap_png.o -lpng

cgl_common_deps=obj/cgl_config.o obj/cgl_string.o obj/cgl_io.o $(datum_deps)

cgl_init_deps=src/cgl_init.c $(cgl_common_deps) -lm
cgl_init: $(cgl_init_deps)
	$(CC) $(FLAGS) -o bin/cgl_init $(cgl_init_deps)

cgl_step_deps=src/cgl_step.c $(cgl_common_deps) obj/cgl_step_io.o $(etd_deps)
cgl_step: $(cgl_step_deps)
	$(CC) $(FLAGS) -o bin/cgl_step $(cgl_step_deps)

cgl_image_deps=src/cgl_image.c $(cgl_common_deps) obj/cgl_image_func.o -lm $(pixmap_deps)
cgl_image: $(cgl_image_deps)
	$(CC) $(FLAGS) -o bin/cgl_image $(cgl_image_deps)

cgl_test_deps=src/cgl_test.c $(cgl_common_deps) obj/cgl_test_func.o $(etd_deps)
cgl_test: $(cgl_test_deps)
	$(CC) $(FLAGS) -o bin/cgl_test $(cgl_test_deps)

cgl_stat_deps=src/cgl_stat.c $(datum_deps) obj/cgl_stat_func.o obj/cgl_config.o obj/cgl_string.o
cgl_stat: $(cgl_stat_deps)
	$(CC) $(FLAGS) -o bin/cgl_stat $(cgl_stat_deps)

obj/%.o: %.c %.h
	$(CC) $(FLAGS) -c $< -o obj/$(@F)

test: cgl_test
	./bin/cgl_test --config src/cgl_test.conf

clean:
	rm -fv bin/* obj/*.o
