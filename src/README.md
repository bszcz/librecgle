### config files

`cgl.conf` - Example config file with comments.

`cgl_test.conf` - Config file for the `cgl_test` unit testing program.


### source files

`cgl_config.c`, `cgl_config.h` - Config file parsing library.

`cgl_config_old.c` - Old but interesting approach to config file parsing. This file is not used.

`cgl.h` - Collective header file for the CGLE utilities.

`cgl_histo.c` - Calculates histogram statistics, useful for convective/absolute instability experiments.

`cgl_image.c` - Entry point for the `cgl_image` visualiser.

`cgl_image_func.c`, `cgl_image_func.h` - Functions for the `cgl_image` utility.

`cgl_init.c` - Entry point for the `cgl_init` initialiser.

`cgl_io.c`, `cgl_io.h` - I/O functions for interfacing between LibreCGLE and Datum (see also `cgl_step_io.c`).

`cgl_stat.c` - Entry point for the `cgl_stat` statistical utility.

`cgl_step.c` - Entry point for the `cgl_step` integrator.

`cgl_stat_func.c`, `cgl_stat_func.h` - Functions for the `cgl_stat` utility.

`cgl_step_io.c`, `cgl_step_io.h` - Additional I/O functions for interfacing between LibreCGLE and Datum (see also `cgl_io.c`).

`cgl_string.c`, `cgl_string.h` - Small string library, used mostly to parse the config file.

`cgl_test.c` - Entry point for the `cgl_test` unit testing utility.

`cgl_test_func.c`, `cgl_test_func.h` - Functions for the `cgl_test` utility.

`etd_create.c`, `etd_create.h` - Part of the ETD library containing function which create and initialise the system.

`etd.h` - Collective header file for the ETD library.

`etd_step.c`, `etd_step.h` - Part or the ETD library containing functions which integrate the system.


### submodules

`datum/`, `pixmap/` - External libraries for data (`datum`) and image (`pixmap`) input/output.
