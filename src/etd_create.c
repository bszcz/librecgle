// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"
#include "etd.h"

static void* etd_fftw_malloc(size_t memsize) {
	void* ptr = fftw_malloc(memsize);
	if (ptr == NULL) {
		printf("etd_fftw_malloc: cannot allocate memory\n");
	}
	return ptr;
}

static void* etd_fftw_free(void* ptr) {
	if (ptr == NULL) {
		printf("etd_fftw_free: freeing NULL pointer\n");
	}
	fftw_free(ptr);
	return NULL;
}

static int etd_init_plans(struct etd_data* etd) {
	fftw_init_threads();
	fftw_plan_with_nthreads(etd->conf.nthreads);
	for (int f = 0; f < etd->conf.nfunc; f++) {
		int gX = etd->conf.gridx;
		int gY = etd->conf.gridy;
		int fftw_flags = etd->conf.fftw_flags;

		etd->plan_zZ   [f] = fftw_plan_dft_2d(gY, gX, etd->z [f], etd->Z  [f], FFTW_FORWARD,  fftw_flags);
		etd->plan_Zz   [f] = fftw_plan_dft_2d(gY, gX, etd->Z [f], etd->z  [f], FFTW_BACKWARD, fftw_flags);
		etd->plan_znZNa[f] = fftw_plan_dft_2d(gY, gX, etd->zn[f], etd->ZNa[f], FFTW_FORWARD,  fftw_flags);
		if (etd->plan_zZ   [f] == NULL ||
		    etd->plan_Zz   [f] == NULL ||
		    etd->plan_znZNa[f] == NULL) {
			printf("etd_init_plans: fftw_plan_dft_2d() returned NULL pointer\n");
			return 1;
		}

		if (etd->conf.need_laplace) {
			etd->plan_ZLzl[f] = fftw_plan_dft_2d(gY, gX, etd->ZL[f], etd->zl[f], FFTW_BACKWARD, fftw_flags);
			if (etd->plan_ZLzl[f] == NULL) {
				printf("etd_init_plans: fftw_plan_dft_2d() returned NULL pointer\n");
				return 1;
			}
		}
	}
	return 0;
}

// Calculates ETD factors for a given location in the grid.
static void etd_init_scheme_factors(struct etd_data* etd, const int g) {
	for (int f = 0; f < etd->conf.nfunc; f++) {
		double lin_re = etd->conf.lincoeffs[f][0] - etd->conf.lapcoeffs[f][0] * etd->ksq[g];
		double lin_im = etd->conf.lincoeffs[f][1] - etd->conf.lapcoeffs[f][1] * etd->ksq[g];
		double mod2_lin  = (lin_re * lin_re) + (lin_im * lin_im);

		// elin = exp(lin * dt)
		double elin_re = etd->elin[f][g][0] = exp(etd->conf.dt * lin_re) * cos(etd->conf.dt * lin_im);
		double elin_im = etd->elin[f][g][1] = exp(etd->conf.dt * lin_re) * sin(etd->conf.dt * lin_im);

		if (etd->conf.order == 1) {
			// etd1a = (elin - 1) / lin
			etd->etd1a[f][g][0] = (elin_im * lin_im + (elin_re - 1.0) * lin_re) / mod2_lin;
			etd->etd1a[f][g][1] = (elin_im * lin_re - (elin_re - 1.0) * lin_im) / mod2_lin;
		}

		if (etd->conf.order == 2) {
			// etd2a = (elin * (1 + 1 / lin / dt) - 1 / lin / dt - 2) / lin
			// etd2b = (elin * (  - 1 / lin / dt) + 1 / lin / dt + 1) / lin
			// NOTE: etd1a == etd2a + etd2b

			double lin_c1 = lin_im * lin_re;
			double lin_c2 = (lin_im + lin_re) * (lin_im - lin_re);

			etd->etd2a[f][g][0] = -1.0 * lin_c2 * (elin_re - 1.0) + 2.0 * lin_c1 * elin_im;
			etd->etd2a[f][g][1] = -2.0 * lin_c1 * (elin_re - 1.0) - 1.0 * lin_c2 * elin_im;
			etd->etd2a[f][g][0] += ((elin_re - 2.0) * lin_re + elin_im * lin_im) * etd->conf.dt * mod2_lin;
			etd->etd2a[f][g][1] -= ((elin_re - 2.0) * lin_im - elin_im * lin_re) * etd->conf.dt * mod2_lin;

			etd->etd2b[f][g][0] = 1.0 * lin_c2 * (elin_re - 1.0) - 2.0 * lin_c1 * elin_im;
			etd->etd2b[f][g][1] = 2.0 * lin_c1 * (elin_re - 1.0) + 1.0 * lin_c2 * elin_im;
			etd->etd2b[f][g][0] += lin_re * etd->conf.dt * mod2_lin;
			etd->etd2b[f][g][1] -= lin_im * etd->conf.dt * mod2_lin;

			double etd2_den = etd->conf.dt * mod2_lin * mod2_lin;

			etd->etd2a[f][g][0] /= etd2_den;
			etd->etd2a[f][g][1] /= etd2_den;

			etd->etd2b[f][g][0] /= etd2_den;
			etd->etd2b[f][g][1] /= etd2_den;
		}
	}
}

// Calculate wavenumbers, dealiasing mask and ETD scheme factors.
static int etd_init_scheme(struct etd_data* etd) {
	// Note: These are absolute values but correct when squared in 'ksq'.
	double* kabsx = etd_fftw_malloc(etd->conf.gridx * sizeof(double));
	double* kabsy = etd_fftw_malloc(etd->conf.gridy * sizeof(double));
	if (kabsx == NULL || kabsy == NULL) {
		printf("etd_init_scheme: cannot allocate memory\n");
		return 1;
	}
	kabsx[0] = 0.0;
	kabsy[0] = 0.0;
	for (int g = 1; g <= etd->conf.gridx / 2; g++) {
		kabsx[g] = kabsx[etd->conf.gridx - g] = 2.0 * ETD_PI * g / etd->conf.domainx;
	}
	for (int g = 1; g <= etd->conf.gridy / 2; g++) {
		kabsy[g] = kabsy[etd->conf.gridy - g] = 2.0 * ETD_PI * g / etd->conf.domainy;
	}
	const double kdealiasx = etd->conf.dealias * (ETD_PI * etd->conf.gridx / etd->conf.domainx);
	const double kdealiasy = etd->conf.dealias * (ETD_PI * etd->conf.gridy / etd->conf.domainy);
	for (int j = 0, g = 0; j < etd->conf.gridy; j++) {
		for (int i = 0; i < etd->conf.gridx; i++, g++) {
			if ((kabsx[i] == kdealiasx && kabsy[j] <= kdealiasy) ||
			    (kabsx[i] <= kdealiasx && kabsy[j] == kdealiasy)) {
				etd->dealias_mask[g] = ETD_DEALIAS_EDGE;
			} else if (kabsx[i] < kdealiasx && kabsy[j] < kdealiasy) {
				etd->dealias_mask[g] = ETD_DEALIAS_KEEP;
			} else {
				etd->dealias_mask[g] = ETD_DEALIAS_LOSE;
			}
			etd->ksq[g] = kabsx[i] * kabsx[i] + kabsy[j] * kabsy[j];
			etd_init_scheme_factors(etd, g);
		}
	}
	etd_fftw_free(kabsx);
	etd_fftw_free(kabsy);
	return 0;
}

// Allocates main arrays for linear, nonlinear and Laplace parts
// in physical and Fourier spaces as well as ETD factors.
static struct etd_data* etd_malloc_main(struct etd_data* etd) {
	size_t memsize = etd->conf.gridpts * sizeof(fftw_complex);
	for (int f = 0; f < etd->conf.nfunc; f++) {
		etd->z   [f] = etd_fftw_malloc(memsize);
		etd->zn  [f] = etd_fftw_malloc(memsize);
		etd->Z   [f] = etd_fftw_malloc(memsize);
		etd->elin[f] = etd_fftw_malloc(memsize);

		if (etd->z   [f] == NULL ||
		    etd->Z   [f] == NULL ||
		    etd->zn  [f] == NULL ||
		    etd->elin[f] == NULL) {
			printf("etd_malloc_main: cannot allocate memory\n");
			return NULL;
		}

		if (etd->conf.need_laplace) {
			etd->zl[f] = etd_fftw_malloc(memsize);
			etd->ZL[f] = etd_fftw_malloc(memsize);
			if (etd->zl[f] == NULL || etd->ZL[f] == NULL) {
				printf("etd_malloc_main: cannot allocate memory\n");
				return NULL;
			}
		}

		if (etd->conf.order == 1) {
			etd->etd1a[f] = etd_fftw_malloc(memsize);
			etd->ZNa[f] = etd_fftw_malloc(memsize);
			if (etd->etd1a[f] == NULL || etd->ZNa[f] == NULL) {
				printf("etd_malloc_main: cannot allocate memory\n");
				return NULL;
			}
		} else if (etd->conf.order == 2) {
			etd->etd2a[f] = etd_fftw_malloc(memsize);
			etd->etd2b[f] = etd_fftw_malloc(memsize);
			etd->ZNa[f] = etd_fftw_malloc(memsize);
			etd->ZNb[f] = etd_fftw_malloc(memsize);
			if (etd->etd2a[f] == NULL || etd->etd2b[f] == NULL ||
			    etd->ZNa  [f] == NULL || etd->ZNb  [f] == NULL) {
				printf("etd_malloc_main: cannot allocate memory\n");
				return NULL;
			}
		}
	}
	return etd;
}

// After allocations in etd_malloc(), pointer arrays are filled with NULL
// to prevent etd_destroy() from deallocating memory from random pointers.
// After calling this function it is safe to pass etd_data struct pointer
// to etd_destroy() at any later time during execution.
static void etd_malloc_null(struct etd_data* etd) {
	for (int f = 0; f < etd->conf.nfunc; f++) {
		if (etd->plan_zZ    != NULL) { etd->plan_zZ   [f] = NULL; }
		if (etd->plan_Zz    != NULL) { etd->plan_Zz   [f] = NULL; }
		if (etd->plan_znZNa != NULL) { etd->plan_znZNa[f] = NULL; }
		if (etd->plan_ZLzl  != NULL) { etd->plan_ZLzl [f] = NULL; }
		if (etd->z   != NULL) { etd->z  [f] = NULL; }
		if (etd->zl  != NULL) { etd->zl [f] = NULL; }
		if (etd->zn  != NULL) { etd->zn [f] = NULL; }
		if (etd->Z   != NULL) { etd->Z  [f] = NULL; }
		if (etd->ZL  != NULL) { etd->ZL [f] = NULL; }
		if (etd->ZNa != NULL) { etd->ZNa[f] = NULL; }
		if (etd->ZNb != NULL) { etd->ZNb[f] = NULL; }
		if (etd->elin  != NULL) { etd->elin [f] = NULL; }
		if (etd->etd1a != NULL) { etd->etd1a[f] = NULL; }
		if (etd->etd2a != NULL) { etd->etd2a[f] = NULL; }
		if (etd->etd2b != NULL) { etd->etd2b[f] = NULL; }
	}
}

// Allocates smaller arrays, see etd_malloc_main() for main allocations.
static struct etd_data* etd_malloc(struct etd_data* etd) {
	etd->dealias_mask = etd_fftw_malloc(etd->conf.gridpts * sizeof(char));
	etd->ksq = etd_fftw_malloc(etd->conf.gridpts * sizeof(double));

	size_t memsize = etd->conf.nfunc * sizeof(fftw_plan);
	etd->plan_zZ    = etd_fftw_malloc(memsize);
	etd->plan_Zz    = etd_fftw_malloc(memsize);
	etd->plan_znZNa = etd_fftw_malloc(memsize);
	etd->plan_ZLzl  = etd_fftw_malloc(memsize);

	memsize = etd->conf.nfunc * sizeof(fftw_complex*);
	etd->z     = etd_fftw_malloc(memsize);
	etd->zn    = etd_fftw_malloc(memsize);
	etd->zl    = etd_fftw_malloc(memsize);
	etd->Z     = etd_fftw_malloc(memsize);
	etd->ZNa   = etd_fftw_malloc(memsize);
	etd->ZNb   = etd_fftw_malloc(memsize);
	etd->ZL    = etd_fftw_malloc(memsize);
	etd->elin  = etd_fftw_malloc(memsize);
	etd->etd1a = etd_fftw_malloc(memsize);
	etd->etd2a = etd_fftw_malloc(memsize);
	etd->etd2b = etd_fftw_malloc(memsize);

	etd_malloc_null(etd); // for clean freeing in etd_destroy()

	// check if memory allocated
	if (etd->dealias_mask == NULL ||
	    etd->plan_zZ    == NULL ||
	    etd->plan_Zz    == NULL ||
	    etd->plan_znZNa == NULL ||
	    etd->plan_ZLzl  == NULL ||
	    etd->ksq == NULL ||
	    etd->z   == NULL ||
	    etd->zn  == NULL ||
	    etd->zl  == NULL ||
	    etd->Z   == NULL ||
	    etd->ZNa == NULL ||
	    etd->ZNb == NULL ||
	    etd->ZL  == NULL ||
	    etd->elin  == NULL ||
	    etd->etd1a == NULL ||
	    etd->etd2a == NULL ||
	    etd->etd2b == NULL) {
		printf("etd_malloc: cannot allocate memory\n");
		return NULL;
	}

	if (etd_malloc_main(etd) == NULL) {
		return NULL;
	}

	return etd;
}

struct etd_data* etd_create(struct config conf) {
	struct etd_data* etd = etd_fftw_malloc(sizeof(struct etd_data));
	if (etd == NULL) {
		printf("etd_create: cannot allocate memory\n");
		return NULL;
	}
	etd->conf = conf;

	printf("etd_malloc: ");
	fflush(stdout);
	if (etd_malloc(etd) == NULL) {
		etd_fftw_free(etd);
		return NULL;
	}
	printf("done\n");

	printf("etd_init_scheme: ");
	fflush(stdout);
	if (etd_init_scheme(etd) != 0) {
		etd_fftw_free(etd);
		return NULL;
	}
	printf("done\n");

	printf("etd_init_plans: ");
	fflush(stdout);
	if (etd_init_plans(etd) != 0) {
		etd_fftw_free(etd);
		return NULL;
	}
	printf("done\n");

	return etd;
}

void* etd_destroy(struct etd_data* etd) {
	for (int f = 0; f < etd->conf.nfunc; f++) {
		if (etd->plan_zZ    != NULL) { fftw_destroy_plan(etd->plan_zZ   [f]); }
		if (etd->plan_Zz    != NULL) { fftw_destroy_plan(etd->plan_Zz   [f]); }
		if (etd->plan_znZNa != NULL) { fftw_destroy_plan(etd->plan_znZNa[f]); }
		if (etd->plan_ZLzl  != NULL) { fftw_destroy_plan(etd->plan_ZLzl [f]); }

		if (etd->z    != NULL) { etd_fftw_free(etd->z [f]); }
		if (etd->zn   != NULL) { etd_fftw_free(etd->zn[f]); }
		if (etd->Z    != NULL) { etd_fftw_free(etd->Z [f]); }
		if (etd->elin != NULL) { etd_fftw_free(etd->elin[f]); }

		if (etd->zl  != NULL) if (etd->zl [f] != NULL) { etd_fftw_free(etd->zl [f]); }
		if (etd->ZL  != NULL) if (etd->ZL [f] != NULL) { etd_fftw_free(etd->ZL [f]); }
		if (etd->ZNa != NULL) if (etd->ZNa[f] != NULL) { etd_fftw_free(etd->ZNa[f]); }
		if (etd->ZNb != NULL) if (etd->ZNb[f] != NULL) { etd_fftw_free(etd->ZNb[f]); }

		if (etd->etd1a != NULL) {
			if (etd->etd1a[f] != NULL) { etd_fftw_free(etd->etd1a[f]); }
		}
		if (etd->etd2a != NULL) {
			if (etd->etd2a[f] != NULL) { etd_fftw_free(etd->etd2a[f]); }
		}
		if (etd->etd2b != NULL) {
			if (etd->etd2b[f] != NULL) { etd_fftw_free(etd->etd2b[f]); }
		}
	}
	etd_fftw_free(etd->dealias_mask);
	etd_fftw_free(etd->ksq);
	etd_fftw_free(etd->plan_zZ);
	etd_fftw_free(etd->plan_Zz);
	etd_fftw_free(etd->plan_znZNa);
	etd_fftw_free(etd->plan_ZLzl);
	etd_fftw_free(etd->z);
	etd_fftw_free(etd->zl);
	etd_fftw_free(etd->zn);
	etd_fftw_free(etd->Z);
	etd_fftw_free(etd->ZL);
	etd_fftw_free(etd->ZNa);
	etd_fftw_free(etd->ZNb);
	etd_fftw_free(etd->elin);
	if (etd->etd1a != NULL) { etd_fftw_free(etd->etd1a); }
	if (etd->etd2a != NULL) { etd_fftw_free(etd->etd2a); }
	if (etd->etd2b != NULL) { etd_fftw_free(etd->etd2b); }
	etd_fftw_free(etd);
	return NULL;
}
