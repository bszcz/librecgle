// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include <fftw3.h>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#include "cgl_config.h"
#include "etd_create.h"
#include "etd_step.h"

static const double ETD_PI = 3.14159265358979323846;

static const int ETD_RE_PART = 0;
static const int ETD_IM_PART = 1;
static const int ETD_COMPLEX = 2;

static const int ETD_DEALIAS_LOSE = 0;
static const int ETD_DEALIAS_KEEP = 1;
static const int ETD_DEALIAS_EDGE = 2;

static const double ETD_GOOD_RESOLUTION = 3.0; // 3 orders of magnitude
