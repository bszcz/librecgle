// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"
#include "etd.h"

void etd_step(const struct etd_data* etd) {
	if (etd->conf.order == 1) {
		#pragma omp parallel for
		for (int g = 0; g < etd->conf.gridpts; g++) {
			if (etd->dealias_mask[g] != ETD_DEALIAS_LOSE) {
				for (int f = 0; f < etd->conf.nfunc; f++) {
					double elin_re = etd->elin[f][g][0];
					double elin_im = etd->elin[f][g][1];
					double etd1a_re = etd->etd1a[f][g][0];
					double etd1a_im = etd->etd1a[f][g][1];
					double Z_re = etd->Z[f][g][0];
					double Z_im = etd->Z[f][g][1];
					double ZNa_re = etd->ZNa[f][g][0];
					double ZNa_im = etd->ZNa[f][g][1];
					etd->Z[f][g][0] = elin_re * Z_re - elin_im * Z_im;
					etd->Z[f][g][1] = elin_re * Z_im + elin_im * Z_re;
					etd->Z[f][g][0] += etd1a_re * ZNa_re - etd1a_im * ZNa_im;
					etd->Z[f][g][1] += etd1a_re * ZNa_im + etd1a_im * ZNa_re;
				}
			}
		}
	} else if (etd->conf.order == 2) {
		#pragma omp parallel for
		for (int g = 0; g < etd->conf.gridpts; g++) {
			if (etd->dealias_mask[g] != ETD_DEALIAS_LOSE) {
				for (int f = 0; f < etd->conf.nfunc; f++) {
					double elin_re = etd->elin[f][g][0];
					double elin_im = etd->elin[f][g][1];
					double etd2a_re = etd->etd2a[f][g][0];
					double etd2a_im = etd->etd2a[f][g][1];
					double etd2b_re = etd->etd2b[f][g][0];
					double etd2b_im = etd->etd2b[f][g][1];
					double Z_re = etd->Z[f][g][0];
					double Z_im = etd->Z[f][g][1];
					double ZNa_re = etd->ZNa[f][g][0];
					double ZNa_im = etd->ZNa[f][g][1];
					double ZNb_re = etd->ZNb[f][g][0];
					double ZNb_im = etd->ZNb[f][g][1];
					etd->Z[f][g][0] = elin_re * Z_re - elin_im * Z_im;
					etd->Z[f][g][1] = elin_re * Z_im + elin_im * Z_re;
					etd->Z[f][g][0] += etd2a_re * ZNa_re - etd2a_im * ZNa_im;
					etd->Z[f][g][1] += etd2a_re * ZNa_im + etd2a_im * ZNa_re;
					etd->Z[f][g][0] += etd2b_re * ZNb_re - etd2b_im * ZNb_im;
					etd->Z[f][g][1] += etd2b_re * ZNb_im + etd2b_im * ZNb_re;
				}
			}
		}
	}
}

void etd_memset(const struct etd_data* etd, fftw_complex** const z, const int half, const double setval) {
	if (half == ETD_COMPLEX) {
		for (int f = 0; f < etd->conf.nfunc; f++) {
			for (int g = 0; g < etd->conf.gridpts; g++) {
				for (int h = 0; h < 2; h++) {
					z[f][g][h] = setval;
				}
			}
		}
	} else if (half == ETD_RE_PART || half == ETD_IM_PART) {
		for (int f = 0; f < etd->conf.nfunc; f++) {
			for (int g = 0; g < etd->conf.gridpts; g++) {
				z[f][g][half] = setval;
			}
		}
	} else {
		printf("etd_memset: specified part(s) of a complex number is(are) invalid\n");
	}
}

void etd_memcpy(const struct etd_data* etd, fftw_complex** restrict const dest, fftw_complex** restrict const src) {
	if (dest == NULL) {
		printf("etd_memcpy: destination pointer dest == NULL\n");
		return;
	}
	if (src == NULL) {
		printf("etd_memcpy: source pointer src == NULL\n");
		return;
	}
	#pragma omp parallel for
	for (int g = 0; g < etd->conf.gridpts; g++) {
		if (etd->dealias_mask[g] != ETD_DEALIAS_LOSE) {
			for (int f = 0; f < etd->conf.nfunc; f++) {
				for (int h = 0; h < 2; h++) {
					dest[f][g][h] = src[f][g][h];
				}
			}
		}
	}
}

void etd_execute(const struct etd_data* etd, fftw_plan* const plans) {
	for (int f = 0; f < etd->conf.nfunc; f++) {
		fftw_execute(plans[f]);
	}
}

void etd_normalise(const struct etd_data* etd, fftw_complex** const Z) {
	#pragma omp parallel for
	for (int g = 0; g < etd->conf.gridpts; g++) {
		if (etd->dealias_mask[g] != ETD_DEALIAS_LOSE) {
			for (int f = 0; f < etd->conf.nfunc; f++) {
				for (int h = 0; h < 2; h++) {
					Z[f][g][h] /= etd->conf.gridpts;
				}
			}
		}
	}
}

void etd_dealias(const struct etd_data* etd, fftw_complex** const Z) {
	#pragma omp parallel for
	for (int g = 0; g < etd->conf.gridpts; g++) { // "g" loop is first
		if (etd->dealias_mask[g] == ETD_DEALIAS_LOSE) { // faster than Z*=D for all values
			for (int f = 0; f < etd->conf.nfunc; f++) {
				for (int h = 0; h < 2; h++) {
					Z[f][g][h] = 0.0;
				}
			}
		}
	}
}

void etd_laplace(const struct etd_data* etd) {
	#pragma omp parallel for
	for (int g = 0; g < etd->conf.gridpts; g++) {
		if (etd->dealias_mask[g] != ETD_DEALIAS_LOSE) {
			for (int f = 0; f < etd->conf.nfunc; f++) {
				for (int h = 0; h < 2; h++) {
					etd->ZL[f][g][h] = (-1.0) * etd->ksq[g] * etd->Z[f][g][h];
				}
			}
		}
	}
}

double etd_resolution(const struct etd_data* etd) {
	// maximum modulus squared of the retained Fourier modes:
	double mod2_max_edge[etd->conf.nthreads]; // edge of the "keep" dealias mask
	double mod2_max_keep[etd->conf.nthreads]; // rest of the "keep" dealias mask
	for (int n = 0; n < etd->conf.nthreads; n++) {
		mod2_max_edge[n] = 0.0;
		mod2_max_keep[n] = 0.0;
	}

	for (int f = 0; f < etd->conf.nfunc; f++) {
		#pragma omp parallel for
		for (int g = 0; g < etd->conf.gridpts; g++) {
			int n = omp_get_thread_num();
			if (etd->dealias_mask[g] != ETD_DEALIAS_LOSE) {
				double mod2 = (
					              etd->Z[f][g][0] * etd->Z[f][g][0] +
					              etd->Z[f][g][1] * etd->Z[f][g][1]
				              );
				if (etd->dealias_mask[g] == ETD_DEALIAS_KEEP) {
					if (mod2 > mod2_max_keep[n]) {
						mod2_max_keep[n] = mod2;
					}
				} else if (etd->dealias_mask[g] == ETD_DEALIAS_EDGE) {
					if (mod2 > mod2_max_edge[n]) {
						mod2_max_edge[n] = mod2;
					}
				}
			}
		}
	}

	// reduce max values from threads
	double mod2_max_edge_all = 0.0;
	double mod2_max_keep_all = 0.0;
	for (int n = 0; n < etd->conf.nthreads; n++) {
		if (mod2_max_edge[n] > mod2_max_edge_all) {
			mod2_max_edge_all = mod2_max_edge[n];
		}
		if (mod2_max_keep[n] > mod2_max_keep_all) {
			mod2_max_keep_all = mod2_max_keep[n];
		}
	}

	const double log10_mod2_range = log10(mod2_max_keep_all) - log10(mod2_max_edge_all);
	return log10_mod2_range / 2.0; // divide by 2.0 to make 'mod2' into 'mod'
}
