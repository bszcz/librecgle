// Copyright 2014-2015 (c) Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

struct cgl_stat {
	double mod2avg;
	double cutoffs[8];
	int core_areas[8];
	int histo_bins[8];
	struct datum* histo_dats[8];
};
typedef struct cgl_stat cgl_stat_s;

void cgl_stat_print_header(cgl_stat_s* stat);

cgl_stat_s* cgl_stat_create(struct datum* dat);

int cgl_stat_pos_max(cgl_stat_s* stat, const int i);

void cgl_stat_core_area(cgl_stat_s* stat);

int cgl_stat_write(cgl_stat_s* stat, char* filename);

void cgl_stat_destroy(cgl_stat_s* stat);
