// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

void cgl_step_save(struct etd_data* cgl, const double t) {
	const int ndims = 3;
	const int parts = 2;
	const int lens[] = { cgl->conf.gridx, cgl->conf.gridy, parts };
	struct datum* dat = datum_alloc(ndims, lens);
	if (dat == NULL) {
		exit(EXIT_FAILURE);
	}
	const int f = 0;
	for (int g = 0, d = 0; g < cgl->conf.gridpts; g++) {
		for (int h = 0; h < 2; h++, d++) {
			dat->data[d] = cgl->z[f][g][h];
		}
	}
	cgl_io_save(dat, cgl->conf, t);
	dat = datum_free(dat);
}

void cgl_step_load(struct etd_data* cgl, const double t) {
	struct datum* dat = cgl_io_load(cgl->conf, t);
	if (dat == NULL) {
		exit(EXIT_FAILURE);
	}
	if (dat->lens[0] != cgl->conf.gridx ||
	    dat->lens[1] != cgl->conf.gridy ||
	    dat->len != 2 * cgl->conf.gridpts) {
		printf("cgl_step_load: cannot fit data into ETD arrays\n");
		exit(EXIT_FAILURE);
	}
	const int f = 0;
	for (int g = 0, d = 0; g < cgl->conf.gridpts; g++) {
		for (int h = 0; h < 2; h++, d++) {
			cgl->z[f][g][h] = dat->data[d];
		}
	}
}
