// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

static void cgl_io_filename(char* filename, struct config conf, const double t) {
	if (streq(conf.inittype, "random")) {
		sprintf(
			filename,
			"%s/cgl_%d__L%.0fx%.0f_G%dx%d__D%.2f_b%.2f_c%.2f__t%06.0f_.%s",
			conf.datapath, conf.initseed,
			conf.domainx, conf.domainy, conf.gridx, conf.gridy,
			conf.D, conf.b, conf.c, t, CGL_FILE_EXT
		);
	} else {
		sprintf(
			filename,
			"%s/cgl__L%.0fx%.0f_G%dx%d__D%.2f_b%.2f_c%.2f__t%06.0f_.%s",
			conf.datapath,
			conf.domainx, conf.domainy, conf.gridx, conf.gridy,
			conf.D, conf.b, conf.c, t, CGL_FILE_EXT
		);
	}
}

int cgl_io_save(const struct datum* dat, const struct config conf, const double t) {
	char filename[CGL_STR_LEN];
	cgl_io_filename(filename, conf, t);
	printf("cgl_io_save: %s\n", filename);
	return datum_write_gz(dat, filename);
}

struct datum* cgl_io_load(struct config conf, const double t) {
	char filename[CGL_STR_LEN];
	cgl_io_filename(filename, conf, t);
	printf("cgl_io_load: %s\n", filename);
	return datum_read_gz(filename);
}
