// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "cgl.h"

void cgl_step_save(struct etd_data* cgl, const double t);

void cgl_step_load(struct etd_data* cgl, const double t);
