// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

static double cgl_image_arg_calc(const double re, const double im) {
	return 180.0 + (atan2(im, re) / CGL_PI * 180.0);
}

static void cgl_image_arg_grey_rgb(const double re, const double im, double* red, double* gre, double* blu) {
	if (re == 0.0 && im == 0.0) { // undefined, set colour to red
		*red = 1.0;
		*gre = 0.0;
		*blu = 0.0;
	} else {
		double arg = cgl_image_arg_calc(re, im);
		*red = arg / 360.0;
		*gre = arg / 360.0;
		*blu = arg / 360.0;
	}
}

static void cgl_image_arg_hue_rgb(const double re, const double im, double* red, double* gre, double* blu) {
	double arg = 0.0;

	if (re == 0.0 && im == 0.0) {
		arg = -1.0; // undefined, set colour to black
	} else {
		arg = cgl_image_arg_calc(re, im);
	}

	if (0.0 <= arg && arg < 60.0) {
		*red = 1.0;
		*gre = arg / 60.0;
		*blu = 0.0;
	} else if (60.0 <= arg && arg < 120.0) {
		*red = (120.0 - arg) / 60.0;
		*gre = 1.0;
		*blu = 0.0;
	} else if (120.0 <= arg && arg < 180.0) {
		*red = 0.0;
		*gre = 1.0;
		*blu = (arg - 120.0) / 60.0;
	} else if (180.0 <= arg && arg < 240.0) {
		*red = 0.0;
		*gre = (240.0 - arg) / 60.0;
		*blu = 1.0;
	} else if (240.0 <= arg && arg < 300.0) {
		*red = (arg - 240.0) / 60.0;
		*gre = 0.0;
		*blu = 1.0;
	} else if (300.0 <= arg && arg <= 360.0) {
		*red = 1.0;
		*gre = 0.0;
		*blu = (360.0 - arg) / 60.0;
	} else { // undefined, set colour to black
		*red = 0.0;
		*gre = 0.0;
		*blu = 0.0;
	}
}

void cgl_image_arg(
	struct pixmap* img, struct datum* dat,
	void (*cgl_image_arg_rgb)(
		const double re, const double im, double* red, double* gre, double* blu
	)
) {
	for (int p = 0, d = 0; p < (img->width * img->height); p += 1, d += 2) {
		double red = 0.0;
		double gre = 0.0;
		double blu = 0.0;
		(*cgl_image_arg_rgb)(dat->data[d + 0], dat->data[d + 1], &red, &gre, &blu);
		img->pixels[p][PIXMAP_RED  ] = 255.0 * red;
		img->pixels[p][PIXMAP_GREEN] = 255.0 * gre;
		img->pixels[p][PIXMAP_BLUE ] = 255.0 * blu;
	}
}

void cgl_image_arg_grey(struct pixmap* img, struct datum* dat) {
	cgl_image_arg(img, dat, cgl_image_arg_grey_rgb);
}

void cgl_image_arg_hue(struct pixmap* img, struct datum* dat) {
	cgl_image_arg(img, dat, cgl_image_arg_hue_rgb);
}

void cgl_image_mod(struct pixmap* img, struct datum* dat) {
	const double max_mod = 1.0;
	double overflow_avg = 0.0;
	int overflow_count = 0;
	for (int p = 0, d = 0; p < (img->width * img->height); p += 1, d += 2) {
		double mod = sqrt(pow(dat->data[d + 0], 2) + pow(dat->data[d + 1], 2));
		if (mod > max_mod) {
			overflow_avg += mod;
			overflow_count++;
			// maximum modulus exceeded, set colour to red
			img->pixels[p][PIXMAP_RED  ] = 255;
			img->pixels[p][PIXMAP_GREEN] = 0;
			img->pixels[p][PIXMAP_BLUE ] = 0;
		} else {
			img->pixels[p][PIXMAP_RED  ] = 255.0 * mod / max_mod;
			img->pixels[p][PIXMAP_GREEN] = 255.0 * mod / max_mod;
			img->pixels[p][PIXMAP_BLUE ] = 255.0 * mod / max_mod;
		}
	}
	if (overflow_count > 0) {
		printf("cgl_image_mod: maximum modulus of %lf exceeded in %.2lf%% of grid points where average modulus is %lf\n",
		       max_mod, 100.0 * (double)overflow_count / (img->width * img->height), overflow_avg / (double)overflow_count);
	}
}

static void cgl_image_part(struct pixmap* img, struct datum* dat, const int part) {
	const double max_abs_val = 1.0;
	double overflow_avg = 0.0;
	int overflow_count = 0;
	for (int p = 0, d = part; p < (img->width * img->height); p += 1, d += 2) {
		double val = dat->data[d];
		if (fabs(val) > max_abs_val) {
			overflow_avg += fabs(val);
			overflow_count++;
			// maximum absolute value exceeded, set colour to white
			img->pixels[p][PIXMAP_RED  ] = 255;
			img->pixels[p][PIXMAP_GREEN] = 255;
			img->pixels[p][PIXMAP_BLUE ] = 255;
		} else if (val >= 0.0) {
			img->pixels[p][PIXMAP_RED  ] = 0.0;
			img->pixels[p][PIXMAP_GREEN] = 0.0;
			img->pixels[p][PIXMAP_BLUE ] = +255.0 * val / max_abs_val;
		} else {
			img->pixels[p][PIXMAP_RED  ] = -255.0 * val / max_abs_val;
			img->pixels[p][PIXMAP_GREEN] = 0.0;
			img->pixels[p][PIXMAP_BLUE ] = 0.0;
		}
	}
	if (overflow_count > 0) {
		printf("cgl_image_part: maximum absolute value of %lf exceeded in %.2lf%% of grid points where average absolute value is %lf\n",
		       max_abs_val, 100.0 * (double)overflow_count / (img->width * img->height), overflow_avg / (double)overflow_count);
	}
}

void cgl_image_re(struct pixmap* img, struct datum* dat) {
	const int part = 0; // real part
	cgl_image_part(img, dat, part);
}

void cgl_image_im(struct pixmap* img, struct datum* dat) {
	const int part = 1; // imag part
	cgl_image_part(img, dat, part);
	// change colours from 'real' to 'imag'
	for (int p = 0; p < (img->width * img->height); p++) {
		if (img->pixels[p][PIXMAP_RED] > 0) {
			img->pixels[p][PIXMAP_BLUE] = img->pixels[p][PIXMAP_RED]; // turn red to magenta
		} else if (img->pixels[p][PIXMAP_BLUE] > 0) {
			img->pixels[p][PIXMAP_GREEN] = img->pixels[p][PIXMAP_BLUE]; // turn blue to cyan
		}
	}
}
