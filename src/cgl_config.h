// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "cgl.h"

struct config {
	int	initseed;
	char	inittype[CGL_STR_LEN];
	char	datapath[CGL_STR_LEN];

	double	D;
	double	b;
	double	c;

	double	domainx;
	double	domainy;
	int	gridx;
	int	gridy;
	int	gridpts;

	double	dealias;
	double	dt;
	int	fftw_flags;
	int	nfunc;
	int	nthreads;
	int	order;
	int	need_laplace;

	double	lincoeffs[1][2];
	double	lapcoeffs[1][2];

	double	savedt;
	int	nsaves;
	double	tstart;

	int	image_arg_grey;
	int	image_arg_hue;
	int	image_mod;
	int	image_re;
	int	image_im;
};

struct config cgl_config_parse(const int argc, char** argv);

int cgl_config_first_file(const int argc, char** argv);

void cgl_config_print(const struct config conf);
