// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

static const int CGL_NOPTS = 10;

struct option {
	void* valp;
	char name[CGL_STR_LEN];
	char type[CGL_STR_LEN];
	int set;
};

static void config_bind_opts(struct option* opts, struct config* pars) {
	int i = 0;
	opts[i++] = (struct option) { (void*)&(pars->D), "D", "double", 0 };
	opts[i++] = (struct option) { (void*)&(pars->b), "b", "double", 0 };
	opts[i++] = (struct option) { (void*)&(pars->c), "c", "double", 0 };
	opts[i++] = (struct option) { (void*)&(pars->domainx), "domainx", "double", 0 };
	opts[i++] = (struct option) { (void*)&(pars->domainy), "domainy", "double", 0 };
	opts[i++] = (struct option) { (void*)&(pars->gridx), "gridx", "int", 0 };
	opts[i++] = (struct option) { (void*)&(pars->gridy), "gridy", "int", 0 };
	opts[i++] = (struct option) { (void*)&(pars->initseed), "initseed", "int", 0 };
	opts[i++] = (struct option) { (void*)&(pars->inittype), "inittype", "char[]", 0 };
	opts[i++] = (struct option) { (void*)&(pars->datapath), "datapath", "char[]", 0 };
}

static void config_set_opt(struct option* opts, char* key, char* val) {
	for (int i = 0; i < CGL_NOPTS; i++) {
		if (streq(opts[i].name, key)) {
			if (streq(opts[i].type, "int")) {
				int* ivalp = (int*)opts[i].valp;
				(*ivalp) = strtol(val, NULL, 10);
			} else if (streq(opts[i].type, "double")) {
				double* dvalp = (double*)opts[i].valp;
				(*dvalp) = strtod(val, NULL);
			} else if (streq(opts[i].type, "char[]")) {
				char* cvalp = (char*)opts[i].valp;
				strncpy(cvalp, val, CGL_STR_LEN);
			}
			opts[i].set++;
		}
	}
}

static int config_check_opts(struct option* opts) {
	int err = 0;
	for (int i = 0; i < CGL_NOPTS; i++) {
		if (opts[i].set == 0) {
			printf("config_check_opts: missing option '%s'\n", opts[i].name);
			err++;
		} else if (opts[i].set > 1) {
			printf("config_check_opts: overwritten option '%s'\n", opts[i].name);
		}
	}
	return err;
}

static void config_print_opts(struct option* opts) {
	printf("config_print_opts:\n");
	for (int i = 0; i < CGL_NOPTS; i++) {
		printf("%s = ", opts[i].name);
		if (streq(opts[i].type, "int")) {
			printf("%d", *(int*)opts[i].valp);
		} else if (streq(opts[i].type, "double")) {
			printf("%lf", *(double*)opts[i].valp);
		} else if (streq(opts[i].type, "char[]")) {
			printf("%s", (char*)opts[i].valp);
		}
		printf("\n");
	}
}
