// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

void cgl_image(
	void (*cgl_image_func)(struct pixmap* img, struct datum* dat),
	struct pixmap* img, struct datum* dat,
	char* filename_head, char* filename_tail
) {
	(*cgl_image_func)(img, dat);
	char filename[CGL_STR_LEN];
	snprintf(filename, CGL_STR_LEN, "%s%s", filename_head, filename_tail);
	pixmap_write_png(img, filename);
	printf("cgl_image: %s\n", filename);
}

int main(int argc, char** argv) {
	struct config conf = cgl_config_parse(argc, argv);
	cgl_config_print(conf);
	const int ff = cgl_config_first_file(argc, argv);
	if (ff == -1) {
		exit(EXIT_FAILURE);
	}

	for (int i = ff; i < argc; i++) {
		printf("\n");
		printf("cgl_image: %s\n", argv[i]);

		char filename_head[CGL_STR_LEN];
		strncpy(filename_head, argv[i], CGL_STR_LEN);
		const int extpos = strlen(filename_head) - strlen(CGL_FILE_EXT) - 1; // -1 for '.'
		filename_head[extpos] = '\0'; // remove extension with '.' before it

		struct datum* dat = datum_read_gz(argv[i]);
		if (dat == NULL) {
			exit(EXIT_FAILURE);
		}
		struct pixmap* img = pixmap_alloc(dat->lens[0], dat->lens[1]);
		if (img == NULL) {
			datum_free(dat);
			exit(EXIT_FAILURE);
		}

		if (conf.image_arg_grey) {
			cgl_image(cgl_image_arg_grey, img, dat, filename_head, "arg_grey.png");
		}
		if (conf.image_arg_hue) {
			cgl_image(cgl_image_arg_hue, img, dat, filename_head, "arg_hue.png");
		}
		if (conf.image_mod) {
			cgl_image(cgl_image_mod, img, dat, filename_head, "mod.png");
		}
		if (conf.image_re) {
			cgl_image(cgl_image_re, img, dat, filename_head, "re.png");
		}
		if (conf.image_im) {
			cgl_image(cgl_image_im, img, dat, filename_head, "im.png");
		}

		pixmap_free(img);
		datum_free(dat);
	}

	exit(EXIT_SUCCESS);
}
