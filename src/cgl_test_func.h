// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "cgl.h"

// Print dealiasing mask for user inspection.
void cgl_test_dealias(struct etd_data* cgl);

int cgl_test_memcpy(struct etd_data* cgl);

int cgl_test_memset(struct etd_data* cgl);

// Excite specific Fourier modes and check them in the Fourier spectrum.
// The modes should appear in the right place and with the right amplitude.
int cgl_test_sincos(struct etd_data* cgl);

// Perform forward and inverse transforms, test for differences in physical space.
int cgl_test_inverse(struct etd_data* cgl);

// Test strings functions from 'cgl_string.c'.
int cgl_test_string(void);
