// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "cgl.h"

int cgl_io_save(const struct datum* dat, const struct config conf, const double t);

struct datum* cgl_io_load(const struct config conf, const double t);
