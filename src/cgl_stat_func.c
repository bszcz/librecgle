// Copyright 2014-2015 (c) Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

void cgl_stat_print_header(cgl_stat_s* stat) {
	printf("# mod2_avg ");
	for (int i = 0; stat->histo_bins[i] != 0; i++) {
		printf("mod2_%d ", stat->histo_bins[i]);
	}
	for (int i = 0; stat->histo_bins[i] != 0; i++) {
		printf("core_%.1f ", stat->cutoffs[i]);
	}
	printf("\n");
}

static double cgl_stat_build_histo(cgl_stat_s* stat, struct datum* dat) {
	double mod2sum = 0.0;
	for (int i = 0; i < dat->len; i += 2) {
		const double re = dat->data[i + 0];
		const double im = dat->data[i + 1];
		const double mod2 = (re * re) + (im * im);
		mod2sum += mod2;
		// populate histograms
		for (int j = 0; stat->histo_bins[j] != 0; j++) {
			int bin_index = (int)(stat->histo_bins[j] * mod2 + 0.5); // + 0.5 for rounding
			if (bin_index < stat->histo_bins[j]) { // ignore mod2 > 1.0
				const int c = 1; // bin count column
				const int k = datum_index(stat->histo_dats[j], bin_index, c);
				stat->histo_dats[j]->data[k] += 1;
			}
		}
	}
	const int grid_pts = dat->lens[0] * dat->lens[1];
	const double mod2avg = mod2sum / (double)grid_pts;
	return mod2avg;
}

cgl_stat_s* cgl_stat_create(struct datum* dat) {
	cgl_stat_s* stat = calloc(1, sizeof(cgl_stat_s));
	if (stat == NULL) {
		printf("cgl_stat_create: cannot allocate memory\n");
		return NULL;
	}

	stat->histo_bins[0] = 100;
	stat->histo_bins[1] = 1000;
	stat->histo_bins[2] = 10000;
	stat->histo_bins[4] = 0; // terminator
	stat->cutoffs[0] = 0.1;
	stat->cutoffs[1] = 0.2;
	stat->cutoffs[2] = 0.3;
	stat->cutoffs[3] = 0.0; // terminator

	for (int i = 0; stat->histo_bins[i] != 0; i++) {
		const int ndim = 2;
		const int cols = 2; // bin position and bin count
		stat->histo_dats[i] = datum_alloc(ndim, (int[]) {stat->histo_bins[i], cols});
		if (stat->histo_dats[i] == NULL) {
			for (int k = 0; k < i; k++) {
				datum_free(stat->histo_dats[i]);
			}
			return NULL;
		}
		// calculate bin positions
		for (int j = 0; j < stat->histo_bins[i]; j++) {
			const int c = 0; // bin position column
			const int k = datum_index(stat->histo_dats[i], j, c);
			stat->histo_dats[i]->data[k] = (double)j / (double)stat->histo_bins[i];
		}
	}

	stat->mod2avg = cgl_stat_build_histo(stat, dat);

	cgl_stat_core_area(stat);

	return stat;
}

int cgl_stat_pos_max(cgl_stat_s* stat, const int i) {
	int max_count = 0;
	int pos_max = 0;
	for (int j = 0; j < stat->histo_bins[i]; j++) {
		const int c = 1; // bin count column
		const int k = datum_index(stat->histo_dats[i], j, c);
		const int count = stat->histo_dats[i]->data[k];
		if (count > max_count) {
			max_count = count;
			pos_max = j;
		}
	}
	return pos_max;
}

void cgl_stat_core_area(cgl_stat_s* stat) {
	const struct datum* histo = stat->histo_dats[0]; // first histogram
	int cutoff_index = 0;
	int core_area = 0;
	for (int i = 0; stat->histo_bins[0]; i++) {
		const int kp = datum_index(histo, i, 0);
		const int kc = datum_index(histo, i, 1);
		const double bin_pos = histo->data[kp];
		const int bin_count = (int)histo->data[kc];
		if (bin_pos >= stat->cutoffs[cutoff_index]) {
			stat->core_areas[cutoff_index] = core_area;
			cutoff_index += 1;
			if (stat->cutoffs[cutoff_index] == 0.0) {
				return;
			}
		}
		core_area += bin_count;
	}
}


int cgl_stat_write(cgl_stat_s* stat, char* filename) {
	const int extpos = strlen(filename) - strlen(CGL_FILE_EXT) - 1; // -1 for '.'
	filename[extpos] = '\0'; // extension and '.' removed, ends with '_'

	for (int i = 0; stat->histo_bins[i] != 0; i++) {
		char histo_filename[CGL_STR_LEN];
		sprintf(histo_filename, "%s_histo_%d_.txt", filename, stat->histo_bins[i]);
		FILE* fp = fopen(histo_filename, "wb");
		if (fp == NULL) {
			printf("cgl_stat_write: cannot write to file '%s'\n", histo_filename);
			return 1;
		}
		for (int j = 0; j < stat->histo_bins[i]; j++) {
			const struct datum* dat = stat->histo_dats[i];
			const int kp = datum_index(dat, j, 0);
			const int kc = datum_index(dat, j, 1);
			const double bin_pos = dat->data[kp];
			const int bin_count = (int)dat->data[kc];
			fprintf(fp, "%.4f %d\n", bin_pos, bin_count);
		}
		fclose(fp);
	}
	return 0;
}

void cgl_stat_destroy(cgl_stat_s* stat) {
	for (int i = 0; stat->histo_bins[i] != 0; i++) {
		datum_free(stat->histo_dats[i]);
	}
	free(stat);
}
