// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

int main(int argc, char** argv) {
	struct config conf = cgl_config_parse(argc, argv);
	cgl_config_print(conf);
	omp_set_num_threads(conf.nthreads);

	struct etd_data* cgl = etd_create(conf);
	if (cgl == NULL) {
		exit(EXIT_FAILURE);
	}

	printf("cgl_test:\n");
	cgl_test_dealias(cgl);
	int err = 0;
	err += cgl_test_memcpy(cgl);
	err += cgl_test_memset(cgl);
	err += cgl_test_sincos(cgl);
	err += cgl_test_inverse(cgl);
	err += cgl_test_string();

	cgl = etd_destroy(cgl);

	if (err == 0) {
		exit(EXIT_SUCCESS);
	} else {
		exit(EXIT_FAILURE);
	}
}
