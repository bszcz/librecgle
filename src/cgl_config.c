// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

void cgl_config_print(const struct config conf) {
	printf("cgl_config_print:\n");

	printf("\tinitseed = %d\n", conf.initseed);
	printf("\tinittype = %s\n", conf.inittype);
	printf("\tdatapath = %s\n", conf.datapath);

	printf("\tD = %lf\n", conf.D);
	printf("\tb = %lf\n", conf.b);
	printf("\tc = %lf\n", conf.c);

	printf("\tdomainx = %lf\n", conf.domainx);
	printf("\tdomainy = %lf\n", conf.domainy);
	printf("\tgridx   = %d\n", conf.gridx);
	printf("\tgridy   = %d\n", conf.gridy);

	printf("\tdt         = %lf\n", conf.dt);
	printf("\tfftw_flags = ");
	if (conf.fftw_flags == FFTW_MEASURE) { // FFTW_MEASURE = 0, can't use bitwise &
		printf("FFTW_MEASURE ");
	}
	if (conf.fftw_flags & FFTW_ESTIMATE) {
		printf("FFTW_ESTIMATE ");
	}
	printf("\n");
	printf("\tnthreads   = %d\n", conf.nthreads);
	printf("\torder      = %d\n", conf.order);

	printf("\tsavedt = %lf\n", conf.savedt);
	printf("\tnsaves = %d\n", conf.nsaves);
	printf("\ttstart = %lf\n", conf.tstart);

	printf("\timage_arg_grey = %d\n", conf.image_arg_grey);
	printf("\timage_arg_hue  = %d\n", conf.image_arg_hue);
	printf("\timage_mod      = %d\n", conf.image_mod);
	printf("\timage_re       = %d\n", conf.image_re);
	printf("\timage_im       = %d\n", conf.image_im);
}

static void cgl_config_set_opt(struct config* confp, char* opt, char* val) {
	if (streq(opt, "initseed")) {
		confp->initseed = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "inittype")) {
		strncpy(confp->inittype, val, CGL_STR_LEN);
		return;
	}
	if (streq(opt, "datapath")) {
		strncpy(confp->datapath, val, CGL_STR_LEN);
		return;
	}

	if (streq(opt, "D")) {
		confp->D = strtod(val, NULL);
		return;
	}
	if (streq(opt, "b")) {
		confp->b = strtod(val, NULL);
		return;
	}
	if (streq(opt, "c")) {
		confp->c = strtod(val, NULL);
		return;
	}

	if (streq(opt, "domainx")) {
		confp->domainx = strtod(val, NULL);
		return;
	}
	if (streq(opt, "domainy")) {
		confp->domainy = strtod(val, NULL);
		return;
	}
	if (streq(opt, "gridx")) {
		confp->gridx = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "gridy")) {
		confp->gridy = strtol(val, NULL, 10);
		return;
	}

	if (streq(opt, "dt")) {
		confp->dt = strtod(val, NULL);
		return;
	}
	if (streq(opt, "fftw_flags")) {
		confp->fftw_flags = 0;
		if (streq(val, "FFTW_ESTIMATE")) {
			confp->fftw_flags |= FFTW_ESTIMATE;
		}
		if (streq(val, "FFTW_MEASURE")) {
			confp->fftw_flags |= FFTW_MEASURE;
		}
		return;
	}
	if (streq(opt, "nthreads")) {
		confp->nthreads = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "order")) {
		confp->order = strtol(val, NULL, 10);
		return;
	}

	if (streq(opt, "savedt")) {
		confp->savedt = strtod(val, NULL);
		return;
	}
	if (streq(opt, "nsaves")) {
		confp->nsaves = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "tstart")) {
		confp->tstart = strtod(val, NULL);
		return;
	}

	if (streq(opt, "image_arg_grey")) {
		confp->image_arg_grey = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "image_arg_hue")) {
		confp->image_arg_hue = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "image_mod")) {
		confp->image_mod = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "image_re")) {
		confp->image_re = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "image_im")) {
		confp->image_im = strtol(val, NULL, 10);
		return;
	}

	printf("cgl_config_set_opt: invalid option '%s'\n", opt);
	exit(EXIT_FAILURE);
}

void cgl_config_usage(char* progname) {
	printf("cgl_config_usage: %s "
	       "--config <config_file> "
	       "opt1=val1 opt2=val2 ... "
	       "--files <data_file_1> <data_file_2> ...\n",
	       progname);
}

int cgl_config_first_file(const int argc, char** argv) {
	for (int i = 0; i < argc - 1; i++) { // argc - 1 since a flag at the end is useless
		if (streq("-f", argv[i]) || streq("--files", argv[i])) {
			return i + 1;
		}
	}
	printf("cgl_config_first_file: no files given\n");
	cgl_config_usage(argv[0]);
	return -1; // files missing if not found in the loop
}

static void cgl_config_parse_pair(struct config* confp, char* pair) {
	strdiv(pair, '#'); // remove comment
	char* val = strtrim(strdiv(pair, '='));
	char* opt = strtrim(pair);
	if (opt && val) {
		cgl_config_set_opt(confp, opt, val);
	} else if ((! opt) && (! val)) {
		// empty line in config file
	} else {
		printf("cgl_config_parse_pair: invalid option ('%s' = '%s')\n", opt, val);
		exit(EXIT_FAILURE);
	}
}

static void cgl_config_parse_file(struct config* confp, char* filename) {
	FILE* fp = fopen(filename, "rb");
	if (fp == NULL) {
		printf("cgl_config_parse_file: cannot open config file '%s'\n", filename);
		exit(EXIT_FAILURE);
	}
	char pair[CGL_STR_LEN] = { 0 };
	while (NULL != fgets(pair, CGL_STR_LEN, fp)) {
		cgl_config_parse_pair(confp, pair);
	}
	fclose(fp);
}

struct config cgl_config_parse(const int argc, char** argv) {
	if (argc == 1) {
		printf("cgl_config_parse: no arguments given\n");
		cgl_config_usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	struct config conf;
	for (int i = 1; i < argc; i++) {
		if (streq("-f", argv[i]) || streq("--files", argv[i])) {
			break;
		}
		if (streq("-c", argv[i]) || streq("--config", argv[i])) {
			if (argc > i + 1) {
				cgl_config_parse_file(&conf, argv[i + 1]);
				i++;
			} else {
				printf("cgl_config_parse: config file name missing\n");
			}
		} else if (streq("-h", argv[i]) || streq("--help", argv[i])) {
			cgl_config_usage(argv[0]);
			exit(EXIT_SUCCESS);
		} else {
			cgl_config_parse_pair(&conf, argv[i]);
		}
	}

	if (conf.initseed == 0) {
		srand(time(NULL));
		conf.initseed = rand();
	}
	conf.gridpts = conf.gridx * conf.gridy;
	conf.need_laplace = 0; // don't need etd->zl/etd->ZL
	conf.dealias = 0.5; // 1/2 dealiasing for cubic CGLE
	conf.nfunc = 1; // only one complex function in CGLE
	const int f = 0; // since nfunc = 1
	conf.lincoeffs[f][0] = 1.0;
	conf.lincoeffs[f][1] = 0.0;
	conf.lapcoeffs[f][0] = conf.D;
	conf.lapcoeffs[f][1] = conf.D * conf.b;
	return conf;
}
