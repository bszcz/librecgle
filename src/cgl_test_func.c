// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

static const double tolerance = 1.0e-12;

void cgl_test_dealias(struct etd_data* cgl) {
	printf("cgl_test_dealias:\n");
	if (cgl->conf.gridx > 128 || cgl->conf.gridy > 64) {
		printf("(dealiasing mask too large for output)\n");
		return;
	}
	for (int g = 0; g < cgl->conf.gridpts; g++) {
		printf("%d", cgl->dealias_mask[g]);
		if ((g + 1) % cgl->conf.gridx == 0) {
			printf("\n");
		}
	}
}

static double cgl_test_rand(const double x) {
	return 4.0 * x * (1.0 - x);
}

static void cgl_test_rand_fill(struct etd_data* cgl, fftw_complex** z) {
	double x = 0.1234;
	const int f = 0;
	for (int g = 0; g < cgl->conf.gridpts; g++) {
		for (int h = 0; h < 2; h++) {
			z[f][g][h] = x;
			x = cgl_test_rand(x);
		}
	}
}

static double cgl_test_max_abs_diff(struct etd_data* cgl, fftw_complex** z1, fftw_complex** z2) {
	double max_abs_diff = 0.0;
	const int f = 0;
	for (int g = 0; g < cgl->conf.gridpts; g++) {
		if (cgl->dealias_mask[g] != ETD_DEALIAS_LOSE) {
			for (int h = 0; h < 2; h++) {
				double diff = z1[f][g][h] - z2[f][g][h];
				if (fabs(diff) > max_abs_diff) {
					max_abs_diff = diff;
				}
			}
		}
	}
	return max_abs_diff;
}

int cgl_test_memcpy(struct etd_data* cgl) {
	printf("cgl_test_memcpy: ");
	fflush(stdout);
	cgl_test_rand_fill(cgl, cgl->z);
	etd_memcpy(cgl, cgl->zn, cgl->z);
	double max_abs_diff = cgl_test_max_abs_diff(cgl, cgl->z, cgl->zn);
	if (max_abs_diff > 0.0) {
		printf("fail\n");
		return 1;
	} else {
		printf("pass\n");
		return 0;
	}
}

int cgl_test_memset(struct etd_data* cgl) {
	printf("cgl_test_memset: ");
	fflush(stdout);
	const double re = 0.1234;
	const double im = 0.5678;
	etd_memset(cgl, cgl->z, ETD_RE_PART, re);
	etd_memset(cgl, cgl->z, ETD_IM_PART, im);
	const long f = 0;
	for (long g = 0; g < cgl->conf.gridpts; g++) {
		if (re != cgl->z[f][g][ETD_RE_PART] ||
		    im != cgl->z[f][g][ETD_IM_PART]) {
			printf("fail\n");
			return 1;
		}
	}
	printf("pass\n");
	return 0;
}

static void cgl_test_sincos_fill(struct etd_data* cgl, const double ampx, const double ampy, const int nx, const int ny) {
	const int f = 0;
	for (int g = 0, gy = 0; gy < cgl->conf.gridy; gy++) {
		for (int gx = 0; gx < cgl->conf.gridx; gx++, g++) {
			double x = (double)gx / (double)cgl->conf.gridx;
			double y = (double)gy / (double)cgl->conf.gridy;
			cgl->z[f][g][0]  = ampx * cos(2.0 * ETD_PI * (double)nx * x);
			cgl->z[f][g][1]  = ampx * sin(2.0 * ETD_PI * (double)nx * x);
			cgl->z[f][g][0] += ampy * cos(2.0 * ETD_PI * (double)ny * y);
			cgl->z[f][g][1] += ampy * sin(2.0 * ETD_PI * (double)ny * y);
		}
	}
}

static int cgl_test_sincos_diff(struct etd_data* cgl, const double ampx, const double ampy, const int nx, const int ny) {
	const int f = 0;
	for (int g = 0, gy = 0; gy < cgl->conf.gridy; gy++) {
		for (int gx = 0; gx < cgl->conf.gridx; gx++, g++) {
			if (cgl->dealias_mask[g] != ETD_DEALIAS_LOSE) {
				if (gx == nx && gy == 0) {
					cgl->Z[f][g][0] -= ampx; // remove excited Fourier modes
				}
				if (gy == ny && gx == 0) {
					cgl->Z[f][g][0] -= ampy; // remove excited Fourier modes
				}
				for (int h = 0; h < 2; h++) {
					if (fabs(cgl->Z[f][g][h]) > tolerance) {
						return 1;
					}
				}
			}
		}
	}
	return 0;
}

int cgl_test_sincos(struct etd_data* cgl) {
	printf("cgl_test_sincos: ");
	fflush(stdout);
	const int maxnx = cgl->conf.dealias * cgl->conf.gridx / 2;
	const int maxny = cgl->conf.dealias * cgl->conf.gridy / 2;
	double ampx = 0.1234;
	double ampy = 0.5678;
	for (int nx = 0; nx < maxnx; nx++) {
		for (int ny = 0; ny < maxny; ny++) {
			ampx = cgl_test_rand(ampx);
			ampy = cgl_test_rand(ampy);
			cgl_test_sincos_fill(cgl, ampx, ampy, nx, ny);
			etd_execute(cgl, cgl->plan_zZ);
			etd_normalise(cgl, cgl->Z);
			if (cgl_test_sincos_diff(cgl, ampx, ampy, nx, ny)) {
				printf("fail\n");
				return 1;
			}
		}
	}
	printf("pass\n");
	return 0;
}

int cgl_test_inverse(struct etd_data* cgl) {
	printf("cgl_test_inverse: ");
	fflush(stdout);

	// prepare physical space
	cgl_test_rand_fill(cgl, cgl->z);
	etd_execute(cgl, cgl->plan_zZ);
	etd_dealias(cgl, cgl->Z);
	etd_normalise(cgl, cgl->Z);
	etd_execute(cgl, cgl->plan_Zz);

	etd_memcpy(cgl, cgl->zn, cgl->z); // save physical space
	etd_execute(cgl, cgl->plan_zZ);
	etd_normalise(cgl, cgl->Z);
	etd_execute(cgl, cgl->plan_Zz);

	double max_abs_diff = cgl_test_max_abs_diff(cgl, cgl->z, cgl->zn);
	if (max_abs_diff > tolerance) {
		printf("fail\n");
		return 1;
	} else {
		printf("pass\n");
		return 0;
	}
}

int cgl_test_string(void) {
	printf("cgl_test_string: ");
	fflush(stdout);

	char trim1[] = " abc ";
	char trim2[] = " \n \r \t ";
	char div1[] = "abc=def";
	char div2[] = "ghi#";
	char div3[] = "#jkl";
	char div4[] = "mno|pqr|stu";
	char* div1d = strdiv(div1, '=');
	char* div2d = strdiv(div2, '#');
	char* div3d = strdiv(div3, '#');
	char* div4d = strdiv(div4, '|');
	char* div4dd = strdiv(div4d, '|');

	if (streq("abc", "abc") &&
	    ! streq("abc", "def") &&
	    streq("abc", strtrim(trim1)) &&
	    NULL == strtrim(trim2) &&
	    NULL == strtrim(NULL) &&
	    streq("abc", div1) &&
	    streq("def", div1d) &&
	    streq("ghi", div2) &&
	    streq("" , div2d) &&
	    streq("" , div3) &&
	    streq("jkl", div3d) &&
	    streq("mno", div4) &&
	    streq("pqr", div4d) &&
	    streq("stu", div4dd)) {
		printf("pass\n");
		return 0;
	} else {
		printf("fail\n");
		return 1;
	}
}
