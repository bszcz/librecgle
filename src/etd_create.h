// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "etd.h"

struct etd_data {
	struct config	conf;

	char*		dealias_mask;
	double*		ksq;

	fftw_complex**	elin;
	fftw_complex**	etd1a;
	fftw_complex**	etd2a;
	fftw_complex**	etd2b;

	fftw_complex**	z;
	fftw_complex**	zn;
	fftw_complex**	zl;
	fftw_complex**	Z;
	fftw_complex**	ZNa;
	fftw_complex**	ZNb;
	fftw_complex**	ZL;

	fftw_plan*	plan_zZ;
	fftw_plan*	plan_Zz;
	fftw_plan*	plan_znZNa;
	fftw_plan*	plan_ZLzl;
};

struct etd_data* etd_create(struct config conf);

void* etd_destroy(struct etd_data* etd);
