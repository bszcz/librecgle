// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

// zn0 = (-1) * (z0 - c*z1) * (z0^2 + z1^2)
// zn1 = (-1) * (z1 + c*z0) * (z0^2 + z1^2)
void cgl_step_nonlin(const struct etd_data* cgl) {
	const int f = 0;
	#pragma omp parallel for
	for (int g = 0; g < cgl->conf.gridpts; g++) {
		double negmodsq = (-1.0) *
		                  (
			                  cgl->z[f][g][0] * cgl->z[f][g][0] +
			                  cgl->z[f][g][1] * cgl->z[f][g][1]
		                  );
		cgl->zn[f][g][0] = negmodsq * (cgl->z[f][g][0] - cgl->conf.c * cgl->z[f][g][1]);
		cgl->zn[f][g][1] = negmodsq * (cgl->z[f][g][1] + cgl->conf.c * cgl->z[f][g][0]);
	}
}

int main(int argc, char** argv) {
	struct config conf = cgl_config_parse(argc, argv);
	cgl_config_print(conf);
	omp_set_num_threads(conf.nthreads);

	struct etd_data* cgl = etd_create(conf);
	if (cgl == NULL) {
		exit(EXIT_FAILURE);
	}
	cgl_step_load(cgl, conf.tstart);
	etd_execute(cgl, cgl->plan_zZ);
	etd_normalise(cgl, cgl->Z);
	etd_dealias(cgl, cgl->Z);

	int firststep = 1;
	for (int i = 1; i < conf.nsaves + 1; i++) {
		double wtime = omp_get_wtime();
		for (int j = 0; j < (int)(conf.savedt / conf.dt); j++) {
			etd_execute(cgl, cgl->plan_Zz);
			cgl_step_nonlin(cgl);
			etd_execute(cgl, cgl->plan_znZNa);
			etd_normalise(cgl, cgl->ZNa);
			etd_dealias(cgl, cgl->ZNa);

			if (conf.order == 2 && firststep) {
				etd_memcpy(cgl, cgl->ZNb, cgl->ZNa);
				firststep = 0;
			}
			etd_step(cgl);
			if (conf.order == 2) {
				etd_memcpy(cgl, cgl->ZNb, cgl->ZNa);
			}

			if (etd_resolution(cgl) < ETD_GOOD_RESOLUTION) {
				printf("cgl_step: etd_resolution == %lf < %lf == ETD_GOOD_RESOLUTION\n",
				       etd_resolution(cgl), ETD_GOOD_RESOLUTION);
			}
		}
		cgl_step_save(cgl, conf.tstart + i * conf.savedt);
		printf("cgl_step: t = %lf ( savedt = %lf, walldt = %lf )\n",
		       conf.tstart + i * conf.savedt, conf.savedt, omp_get_wtime() - wtime);
	}

	cgl = etd_destroy(cgl);
	exit(EXIT_SUCCESS);
}
