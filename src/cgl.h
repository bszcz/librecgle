// Copyright (c) 2013, 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include <ctype.h>
#include <fftw3.h>
#include <float.h>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "datum/datum_gz.h"
#include "pixmap/pixmap_png.h"

#define CGL_STR_LEN 512

#include "etd.h"

#include "cgl_config.h"
#include "cgl_image_func.h"
#include "cgl_io.h"
#include "cgl_stat_func.h"
#include "cgl_step_io.h"
#include "cgl_string.h"
#include "cgl_test_func.h"

static const double CGL_PI = 3.14159265358979323846;
static const char CGL_FILE_EXT[] = "datum.gz";
