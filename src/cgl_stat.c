// Copyright 2014-2015 (c) Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

int main(int argc, char** argv) {
	const int ff = cgl_config_first_file(argc, argv);
	if (ff == -1) {
		printf("cgl_stat_usage: %s -f <data_file_1> <data_file_2> ...\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	for (int i = ff; i < argc; i++) {
		char* filename = argv[i];

		struct datum* dat = datum_read_gz(filename);
		if (dat == NULL) {
			exit(EXIT_FAILURE);
		}
		cgl_stat_s* stat = cgl_stat_create(dat);
		datum_free(dat);
		if (stat == NULL) {
			exit(EXIT_FAILURE);
		}

		if (i == ff) { // print only once
			cgl_stat_print_header(stat);
		}

		printf("%f ", stat->mod2avg);

		for (int i = 0; stat->histo_bins[i] != 0; i++) {
			const int pos_max = cgl_stat_pos_max(stat, i);
			const double max_mod2 = (double)pos_max / (double)stat->histo_bins[i];
			printf("%f ", max_mod2);
		}
		for (int i = 0; stat->cutoffs[i] != 0.0; i++) {
			printf("%8d ", stat->core_areas[i]);
		}
		printf("\n");

		int write_err = cgl_stat_write(stat, filename);
		cgl_stat_destroy(stat);
		if (write_err != 0) {
			exit(EXIT_FAILURE);
		}
	}

	exit(EXIT_SUCCESS);
}
