// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "etd.h"

void etd_step(const struct etd_data* etd);

void etd_memset(const struct etd_data* etd, fftw_complex** const z, const int half, const double setval);

void etd_memcpy(const struct etd_data* etd, fftw_complex** restrict const dest, fftw_complex** restrict const src);

void etd_execute(const struct etd_data* etd, fftw_plan* const plans);

void etd_normalise(const struct etd_data* etd, fftw_complex** const Z);

void etd_dealias(const struct etd_data* etd, fftw_complex** const Z);

void etd_laplace(const struct etd_data* etd);

double etd_resolution(const struct etd_data* etd);
