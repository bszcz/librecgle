// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cgl.h"

void cgl_init_rand(struct datum* dat, const int initseed, const double ampl) {
	srand(initseed);
	for (int i = 0; i < dat->len; i++) {
		dat->data[i] = ampl - (2 * ampl * (double)rand() / (double)RAND_MAX);
	}
}

void cgl_init_wave(struct datum* dat, const double ampl) {
	const int width = dat->lens[0];
	const double n = 8.0;
	for (int i = 0; i < width; i++) {
		const int re = (2 * i) + 0;
		const int im = (2 * i) + 1;
		const double x = (double)i / (double)width;
		dat->data[re] = ampl * cos(2.0 * 3.14159 * n * x);
		dat->data[im] = ampl * sin(2.0 * 3.14159 * n * x);
	}
}

void cgl_init_pair(struct datum* dat, const int width, const double ampl) {
	for (int i = 0; i < dat->len; i += 2) {
		dat->data[i + 0] = ampl;
		dat->data[i + 1] = 0.0;
	}
	const int height = 32;
	int start = dat->len / 2;       // move to centre
	start -= dat->lens[0] * height; // move up
	start += dat->lens[0] - width;  // move left
	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			int index = start + 2 * (j * dat->lens[0] + i);
			dat->data[index + 0] = ampl * cos(2.0 * 3.14159 * (double)j / (double)height);
			dat->data[index + 1] = ampl * sin(2.0 * 3.14159 * (double)j / (double)height);
		}
	}
}

int main(int argc, char** argv) {
	struct config conf = cgl_config_parse(argc, argv);
	cgl_config_print(conf);

	const int ndims = 3;
	const int parts = 2;
	const int lens[] = { conf.gridx, conf.gridy, parts };
	struct datum* dat = datum_alloc(ndims, lens);
	if (dat == NULL) {
		exit(EXIT_FAILURE);
	}

	const double ampl = 0.25;
	if (streq("random", conf.inittype)) {
		cgl_init_rand(dat, conf.initseed, ampl);
	} else if (streq("wave", conf.inittype)) {
		cgl_init_wave(dat, ampl);
	} else if (streq("pair", conf.inittype)) {
		cgl_init_pair(dat, conf.gridx / 2, ampl);
	} else {
		printf("cgl_init: invalid initialisation type '%s'\n", conf.inittype);
	}

	const double inittime = 0.0;
	cgl_io_save(dat, conf, inittime);
	datum_free(dat);
	exit(EXIT_SUCCESS);
}
