// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "cgl.h"

void cgl_image_arg_grey(struct pixmap* img, struct datum* dat);

void cgl_image_arg_hue(struct pixmap* img, struct datum* dat);

void cgl_image_mod(struct pixmap* img, struct datum* dat);

void cgl_image_re(struct pixmap* img, struct datum* dat);

void cgl_image_im(struct pixmap* img, struct datum* dat);
